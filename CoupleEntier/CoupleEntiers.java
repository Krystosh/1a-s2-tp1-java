package CoupleEntier;
class CoupleEntiers {
    private int premier;
    private int second;
    
    public CoupleEntiers() {
        this.premier = 0;
        this.second = 0;
    }
    public CoupleEntiers(int premier,int second) {
        this.premier = premier;
        this.second = second;
    }
    public void setPrem(int premier) {this.premier = premier;}
    public void setSec(int second) {this.second = second;}

        public void permute() {
            int aux;
            aux = this.premier;
            this.premier = this.second;
            this.second = aux;
    }
    public int fraction() {
        if (this.second != 0) {
            return this.premier / this.second;
        }
        return 0;
    }

    public String toString() {
        return "Le résultat est " + premier + " " +second;
    }
    
    public int getPrem() {
        return this.premier;
    }

    public int somme() {
        return this.premier + this.second;
    }

    public int getSec() {
        return this.second;
    }

    public CoupleEntiers plus(CoupleEntiers C1, CoupleEntiers C2){
        CoupleEntiers res = new CoupleEntiers();
        res.setPrem(C1.getPrem()+C2.getPrem());
        res.setSec(C1.getPrem()+C2.getSec());
        return res;
    }

}