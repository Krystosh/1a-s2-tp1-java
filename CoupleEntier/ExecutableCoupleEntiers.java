package CoupleEntier;
class ExecutableCoupleEntiers {
    public static void main(String [] args) {
        CoupleEntiers couple = new CoupleEntiers();
        couple.setPrem(5);
        CoupleEntiers couple2 = new CoupleEntiers();
        couple2.setPrem(5);
        couple2.setSec(5);

        System.out.println("Premier couple " + couple); // (1)
        System.out.println(" ");

        System.out.println("Le résultat de la fraction est "+couple.fraction()); // (2)
        System.out.println(" ");

        couple.setSec(5);
        System.out.println("Voici le couple "+couple.getPrem()+" "+couple.getSec());
        System.out.println(" ");

        System.out.println("Ceci est la somme du couple "+couple.somme()); // affiche -22
        System.out.println(" ");

        System.out.println("Ici cest cool "+couple.plus(couple,couple2));
        System.out.println(" ");

        CoupleEntiers couple4 = new CoupleEntiers(7,8);
        System.out.println(couple4);   
    }
}