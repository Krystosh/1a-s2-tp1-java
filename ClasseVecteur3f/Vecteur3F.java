package ClasseVecteur3f;

class Vecteur3f {
    private double x;
    private double y;
    private double z;

    public Vecteur3f(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z= z;
    }

    public String toString() {
        return "<"+x+" "+y+" "+z+">" + " De norme : " + Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)+Math.pow(z, 2));
    }
}
