package ClasseComplexe;

class Complexe {

    private int reel;
    private int image;

    public Complexe(int reel,int image) {

        this.reel = reel;
        this.image = image;
    }

    public int getReel() {
        return this.reel;
    }

    public int getImag() {
        return this.image;
    }

    public String afficheCartesien() {
        return "("+this.reel+ ", " + this.image+")";
    }

    public String afficheComplexe() {
        return this.reel + " +i " + this.image;
    }

    public void setCouple(int reel, int image) {
        this.reel = reel;
        this.image = image;
    }

    public String toString() {
        return "Le resultat est " + reel + " " + image;
    }

    public Complexe sommeComplexe(Complexe C1, Complexe C2){
        Complexe res = new Complexe(0,0);
        res.setCouple(C1.getReel()+C2.getReel(),C1.getImag()+C2.getImag());
        return res;
    }

    public boolean egal(Complexe C1, Complexe C2) {
        if (C1.getReel() == C2.getReel() || C1.getImag() == C2.getImag()) {
            return true;
        }
        return false;
    }
}
