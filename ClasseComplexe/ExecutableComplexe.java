package ClasseComplexe;

public class ExecutableComplexe {
    public static void main(String [] args) {
        Complexe complexe1 = new Complexe(5, -6);
        Complexe complexe2 = new Complexe(2, -4);
        Complexe complexe3 = new Complexe(2, -4);
        Complexe complexe4 = new Complexe(4/2, -4);

        System.out.println(complexe1.getReel()+ " "+ complexe2.getImag());


        System.out.println(complexe1.afficheCartesien());
        // affiche (5, -6)


        System.out.println(complexe2.afficheComplexe());
        // affiche 2 +i -4

        System.out.println(complexe1.sommeComplexe(complexe1,complexe2));

        System.out.println(complexe1.egal(complexe3, complexe4));
        System.out.println(complexe2 == complexe3); //false car il ne font pas référence à la même instance string
        System.out.println(complexe3.equals(complexe4));
    }
}